public enum Servicio{
    A("cambio de aceite",200),
    B("arreglo de frenos",500),
    C("llantas",250),
    D("hojalateria",100);

    String nombre;
    double costo;

    Servicio(String nombre,double costo){
        setNombre(nombre);
        setCosto(costo);
    }

    public void setNombre(String nombre){
        this.nombre = nombre;
    }

    public String getNombre(){
        return this.nombre;
    }

    public void setCosto(double costo){
        this.costo = costo;
    }

    public double getCosto(){
        return this.costo;
    }

} 