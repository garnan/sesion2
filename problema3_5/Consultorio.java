public class Consultorio{

    public static void main(String[] args) {
        
        PacienteBean pacientes [] = new PacienteBean[8];

        String nombres [] = { 
            "Ricardo",
            "Raul",
            "Ivan",
            "Mario",
            "David",
            "Mateo",
            "Juan",
            "Gustavo"
        };

        String apellidos [] = {
            "Pacheco",
            "Paul",
            "Ivankov",
            "Bros",
            "Mercado",
            "Paredes",
            "Ramos",
            "Gustav"
        };

        int edades[] = {
            23,
            25,
            26,
            28,
            21,
            22,
            25,
            27
        };

        String sintomas [] = {
            "tos",
            "fiebre",
            "dolor de estomago",
            "dolor de cabeza",
            "gripa",
            "fractura",
            "lesion",
            "cortaduras"
        };

        for(int i=0;i<pacientes.length;i++){
            pacientes[i] = new PacienteBean();
            pacientes[i].setNombre(nombres[i]);
            pacientes[i].setApellido(apellidos[i]);
            pacientes[i].setEdad(edades[i]);
            pacientes[i].setSintoma(sintomas[i]);
        }
        

        for (PacienteBean p : pacientes) {
            System.out.println(p);
        }

    }

}