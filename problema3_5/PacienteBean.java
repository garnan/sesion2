public class PacienteBean{

    private String nombre;
    private String apellido;
    private int edad;
    private String sintoma;

    public PacienteBean(){}

    public PacienteBean(String nombre,String apellido,int edad,String sintoma){
        setNombre(nombre);
        setApellido(apellido);
        setEdad(edad);
        setSintoma(sintoma);
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public String getNombre(){
        return this.nombre;
    }

    public void setApellido(String apellido)
    {
        this.apellido = apellido;
    }

    public String getApellido(){
        return this.apellido;
    }

    public void setEdad(int edad)
    {
        this.edad = edad;
    }

    public int getEdad(){
        return this.edad;
    }

    public void setSintoma(String sintoma)
    {
        this.sintoma = sintoma;
    }

    public String getSintoma(){
        return this.sintoma;
    }

    @Override
    public String toString() {
        
        return "Paciente { nombre: "+this.getNombre()+", apellido: "+this.getApellido()+", edad "+this.getEdad()+", sintoma: "+this.getSintoma()+" }";

    }


}