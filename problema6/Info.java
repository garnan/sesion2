public class Info{

    class Laptop extends Dispositivo {

        public Laptop(String marca, String modelo, int anio) {
            super(marca, modelo, anio);
        }

    }

    public Info(){

        Laptop laptop = new Laptop("Asus", "Rog", 2015);
        System.out.println("informacion de la laptop");
        System.out.println(laptop); 
    }

    public static void main(String[] args) {
        
           new Info();    
    }

}