public class Dispositivo{

    protected String marca,modelo;
    protected int anio;

    public Dispositivo(String marca,String modelo,int anio){
        this.marca = marca;
        this.modelo = modelo;
        this.anio = anio;
    }

    @Override
    public String toString() {
        return "{ marca: "+this.marca+", modelo: "+this.modelo+", anio: "+this.anio+" }";
    }


}