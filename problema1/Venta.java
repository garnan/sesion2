public class Venta{

    double subtotal(double [] nums){

        double sum = 0;

        for( double i : nums )
            sum += i;

        return sum;
    }

    double iva(double subtotal){
        return subtotal * 0.16;
    }

    Venta(){
        double nums [] = new double [5];

        System.out.println("numeros de la tabla");
        for (int i=0;i<nums.length;i++){
            nums[i] =  4.0 + ( Math.random() * 10 );
            System.out.println(nums[i]);
        }

        double subtotal = subtotal(nums);
        System.out.println("subtotal de los numeros");
        System.out.println(subtotal);
        System.out.println("iva");
        double iva = iva(subtotal);
        System.out.println(iva);

        System.out.println("total de los valores");
        System.out.println( (subtotal+iva) );

    }    

    public static void main(String [] args){
        new Venta();
    }

}